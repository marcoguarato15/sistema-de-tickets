//carregando modulos
const express = require('express')
const mongoose = require('./src/config/banco')
const app = express();
const path = require('path')
const handlebars = require('express-handlebars')
const ticket = require('./src/tickets/routes')
const session = require('express-session')
const flash = require('connect-flash')
const client = require('./src/client/routes')

//Configurações

//sessao
app.use(session({
    secret: "sistemadetickets",
    resave: true,
    saveUninitialized: true
}))
app.use(flash())

//Midleware
app.use((req, res, next) => {
    res.locals.success_msg = req.flash("success_msg")
    res.locals.error_msg = req.flash("erros_msg")
    next()
})

//body-parser
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
//express=handlebars
app.engine('handlebars', handlebars({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars')

//Public
//Pasta estatica do pro Public
app.use(express.static(path.join(__dirname, "public")))

//Rotas
//utilizando roteador
app.use('/ticket', ticket)
app.use('/clientes', client)

app.use('/', (req, res) => {
    res.render("principal/index")
})





const PORT = 3000
app.listen(PORT, () => {
    console.log("Servidor Rodando")
})



/*
//async function server() {
    //Configurações
    // Sessão
    app.use(session({
        secret: "tickets",
        resave: true,
        saveUninitialized: true
    }))
    app.use(flash())

    //middleware
    app.use((req,res,next)=>{
        res.locals.success_msg = req.flash("success_msg")
        res.locals.error_msg = req.flash("error_msg")
        next()
    })
    //body-parser
    app.use(express.json())
    app.use(express.urlencoded({
        extended: true,
    }))
    //express=handlebars
    app.engine('handlebars', handlebars({ defaultLayout: 'main' }))
    app.set('view engine', 'handlebars')

    //Public
    //Pasta estatica do pro Public
    app.use(express.static(path.join(__dirname, "public")))

    //Mongoose connection
    await mongoose.connect('mongodb+srv://marco-antonio:marcoantonio@clusterdaw.69x6d.mongodb.net/test?authSource=admin&replicaSet=atlas-nx122y-shard-0&readPreference=primary&ssl=true')
        .then(() => {
            console.log("Conexao com o banco estabelecida")
        })
        .catch((err) => {
            console.log("Erro ao se conectar com o banco" + err)
        })

    //Rotas
    //utilizando roteador
    app.use('/ticket', ticket)
    //app.use('/user', user)
    app.get('/', (req, res) => {
        res.render('principal/index')
    })




    //Outros
    const PORT = 3000;
    var server = app.listen(PORT, () => {
        console.log("Servidor rodando")

        process.on('SIGINT', async () => {
            console.log('kil')
            server.close()
            await mongoose.disconnect()
            process.exit()
        })

    })
}

server()
*/

module.exports = app;
