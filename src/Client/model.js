const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Cliente = new Schema({
    nome: {
        type: String,
        required: true
    },
    cpf: {
        type: String,
        required: false
    },
    cpjn: {
        type: String,
        required: false
    },
    telefone: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: true
    },
    endereco: {
        type: String,
        required: false
    },
    bairro: {
        type: String,
        required: false
    },
    cidade: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

mongoose.model("clientes", Cliente)