const express = require('express')
const router = express.Router()
const controler = require('./controller')

//pagina principal de clientes
router.get("/", controler.Listar)

//abrir a pagina de cadastro de ciente
router.get("/add", (req, res) => {
    res.render("clientes/addclientes")
})

//cadastrar cliente
router.post("/novo", controler.Criar)

//


module.exports = router