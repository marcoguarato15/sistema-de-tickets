//const { urlencoded } = require('body-parser')
const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
require('../client/model')
const Cliente = mongoose.model('clientes')

//inicio metodo criar clientes
exports.Criar = (req, res) => {
    var erros = []
    var checkbox


    if (!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
        erros.push({ texto: "Nome inválido!" })
    }
    /*
        if (req.body.frPessoaFisica == cheked) {
            if (req.body.cpf.length != 9 || !req.body.cpf) {
                erros.push({ texto: "Cpf incorreto" })
            }
        } else {
            if (req.body.cnpj.length != 14 || !req.body.cnpj) {
                erros.push({ texto: "Cnpj inválido!" })
            }
        }*/

    if (!req.body.telefone || /*req.body.telefone > 11 || */typeof req.body.telefone == undefined) {
        erros.push({ texto: "Telefone inválido" })
    }

    if (!req.body.email || typeof req.body.email == undefined) {
        erros.push({ texto: "E-mail inválido" })
    }

    if (!req.body.endereco || req.body.endereco < 4 || typeof req.body.endereco == undefined) {
        erros.push({ texto: "Endereço inválido" })
    }
    if (!req.body.bairro || req.body.bairro < 4 || typeof req.body.bairro == undefined) {
        erros.push({ texto: "Bairro inválido" })
    }
    if (!req.body.cidade || req.body.cidade < 3 || typeof req.body.cidade == undefined) {
        erros.push({ texto: "Cidade inválida" })
    }

    if (erros.length > 0) {
        res.render("clientes/addclientes", { erros: erros })
    } else {
        const novoCliente = {
            nome: req.body.nome,
            cpf: req.body.cpfcnpj,
            telefone: req.body.telefone,
            email: req.body.email,
            endereco: req.body.endereco,
            bairro: req.body.bairro,
            cidade: req.body.cidade
        }

        /*
                if (req.body.frPessoaFisica == cheked) {
                    const novoCliente = {
                        nome: req.body.nome,
                        cpf: req.body.cpfcnpj,
                        telefone: req.body.telefone,
                        email: req.body.email,
                        endereco: req.body.endereco,
                        bairro: req.body.bairro,
                        cidade: req.body.cidade
                    }
                } else {
                    const novoCliente = {
                        nome: req.body.nome,
                        cnpj: req.body.cpfcnpj,
                        telefone: req.body.telefone,
                        email: req.body.email,
                        endereco: req.body.endereco,
                        bairro: req.body.bairro,
                        cidade: req.body.cidade
                    }
                }*/

        new Cliente(novoCliente).save().then(() => {
            req.flash("success_msg", "Cliente Salvo com sucesso")
            res.redirect("./")
        }).catch((err) => {
            req.flash("error_msg", "Houve um erro ao salvar o cliente, tente novamente")
            res.redirect("./")
        })
    }
}

//fim metodo criar clientes

//inicio metodo listar clientes
exports.Listar = (req, res) => {
    Cliente.find().lean().then((clientes) => {
        res.render("clientes/clientes", { clientes: clientes })
    }).catch((err) => {
        req.flash("error_msg", "Houve um erro ao listar os clientes")
        res.redirect("./")
    })
}
//fim metodo listar