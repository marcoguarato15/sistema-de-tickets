const mongoose = require('mongoose')
//Mongoose connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb+srv://marco-antonio:marcoantonio@clusterdaw.69x6d.mongodb.net/sistemadetickets?authSource=admin&replicaSet=atlas-nx122y-shard-0&readPreference=primary&ssl=true')
    .then(() => {
        console.log("Conexao com o banco estabelecida")
    })
    .catch((err) => {
        console.log("Erro ao se conectar com o banco" + err)
    })

module.exports = mongoose
