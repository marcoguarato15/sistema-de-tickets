const Tickets = require('./model')
const express = require('express')
const router = express.Router()


exports.create = (req, res) => {

    console.log(req.body)

    var erros = []

    if(req.body.categoria == "outros"){
        if(req.body.problema.trim() == "" || req.body.problema.length < 5){
            erros.push({texto: "De uma identificação ao Problema!"})
        }
    }

    if(erros.length > 0){
        res.render('ticket/novo', {erros: erros})
    }
    else{

    Tickets.create(req.body)
    .then(() => {
        req.flash("success_msg", "Ticket adicionado com sucesso!")
        res.redirect('/ticket')
        
    }).catch((err) => {
        console.log('Erro ao salvar ticket: ' + err)
    })
    }
}

exports.find = (req, res) => {
    Tickets.find()
        .then((tickets) => {
            res.json(tickets)
        })
        .catch((err) => {
            console.log("Erro ao carregar documentos!" + err)
        })
}