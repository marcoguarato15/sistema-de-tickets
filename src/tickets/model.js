const mongoose = require('mongoose')

const TicketSchema = mongoose.Schema({
  idcliente: {
    type: Number,
    required: true,
    default: 0//id do cliente logado
  },
  idsuporte: {
    type: Number,
    required: false,
    default: 0, //alterar para o id do gerenciador de tickets
  },
  categoria: {
      type: String,
      required: true
  },
  dataAbertura: {
    type: Date,
    default: Date.now()
  },
  dataFinalizacao: {
    type: Date,
    default: Date.now()
  },
  problema: {
    type: String,
    required: false
  }
})

module.exports = mongoose.model('tickets', TicketSchema);