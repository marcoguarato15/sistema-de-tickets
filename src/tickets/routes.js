const express = require('express')
const router = express.Router()
const controller = require('./controller')

    //cria um novo ticket
    router.get('/', (req,res)=>{
        res.render('ticket/index')
    })

    router.get('/novo', (req,res)=>{
        res.render('ticket/novo')
    })

    router.post('/', controller.create)

    router.get('/tickets', controller.find)
    
    router.delete('')

module.exports = router
